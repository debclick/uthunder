sed -i "/^Terminal=/i Icon=share/icons/hicolor/128x128/apps/thunderbird.png" ${INSTALL_DIR}/thunderbird.desktop
#a short description for the package
sed -i "s|@description@|thunderbird is a webbrowser|" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
rm ${CLICK_PATH}/thunderbird
cd ${CLICK_PATH}
ln -s ../thunderbird/thunderbird .
cp ${BUILD_DIR}/immodules.cache ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_PATH}/thunderbird.sh
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/immodules.cache
make -C ${ROOT}/mobile-config-thunderbird/ DESTDIR=${INSTALL_DIR} THUNDERBIRD_CONFIG_DIR=etc/thunderbird THUNDERBIRD_DIR=lib/${ARCH_TRIPLET}/thunderbird install
